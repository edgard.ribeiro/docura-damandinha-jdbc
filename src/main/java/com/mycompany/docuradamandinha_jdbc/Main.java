package com.mycompany.docuradamandinha_jdbc;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;


public class Main extends Application {
	 
	@Override
    public void start(Stage primaryStage) {

        try {
            // Carrega o layout FXML
            Pane root = FXMLLoader.load(getClass().getResource("/fxml/MainView.fxml"));
            // Cria a cena
            Scene scene = new Scene(root);
            // Define parâmetros para a janela
            primaryStage.setMinWidth(900);
            primaryStage.setMinHeight(500);
            primaryStage.getIcons().add(new Image("/fxml/image/unnamed.png"));
            primaryStage.setTitle("Sistema de dados Doçura D'Amandinha");
            primaryStage.setScene(scene);
            primaryStage.setResizable(true);
            primaryStage.show();
        } catch (Exception erro) {
            erro.printStackTrace();
        }
    }	
	
    public static void main( String[] args ) {
        launch(args);
    }
}
